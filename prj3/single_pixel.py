import foolbox
import keras
import numpy as np
from keras.applications.resnet50 import ResNet50
from keras.applications.resnet50 import preprocess_input
from keras.applications.resnet50 import decode_predictions
import cv2 as cv

# instantiate model
keras.backend.set_learning_phase(0)
kmodel = ResNet50(weights='imagenet')
preprocessing = (np.array([104, 116, 123]), 1)
fmodel = foolbox.models.KerasModel(kmodel, bounds=(0, 255), preprocessing=preprocessing)

# get source image and label
#image, label = foolbox.utils.imagenet_example()
images, labels = foolbox.utils.samples(index=5, batchsize=10, shape=(60,60))

for image, label in zip(images, labels):
    # apply attack on source image
    # ::-1 reverses the color channels, because Keras ResNet50 expects BGR instead of RGB
    attack = foolbox.attacks.SinglePixelAttack(fmodel)
    image = cv.resize(image,(224,224))
    adversarial = attack(image[:, :, ::-1], label, max_pixels=100)
    #print(adversarial)
    # if the attack fails, adversarial will be None and a warning will be printed


    #find class of image
    class_num = np.argmax(fmodel.predictions(image))
    print("Numerical image class:", np.argmax(fmodel.predictions(image)))
    print("Accuracy of prediction:", foolbox.utils.softmax(fmodel.predictions(image))[class_num])
    image_rgb = image[np.newaxis, :, :, ::-1]
    preds1 = kmodel.predict(preprocess_input(image_rgb.copy()))
    #print("Top prediction image: ", decode_predictions(preds1, top=5))
    print("Top prediction image: ", decode_predictions(preds1, top=5)[0][0][1])

    #find class of adversarial
    class_num = np.argmax(fmodel.predictions(adversarial))
    print("Numerical adversarial class:", np.argmax(fmodel.predictions(adversarial)))
    print("Accuracy of prediction:", foolbox.utils.softmax(fmodel.predictions(adversarial))[class_num])
    adversarial_rgb = adversarial[np.newaxis, :, :, ::-1]
    preds = kmodel.predict(preprocess_input(adversarial_rgb.copy()))
    #print("Top prediction adversarial: ", [x for x in decode_predictions(preds, top=5)[0]])
    print("Top prediction adversarial: ", decode_predictions(preds, top=5)[0][0][1])

    import matplotlib.pyplot as plt

    plt.figure()

    plt.subplot(1, 3, 1)
    plt.title('Original')
    plt.imshow(image / 255)  # division by 255 to convert [0, 255] to [0, 1]
    plt.axis('off')

    plt.subplot(1, 3, 2)
    plt.title('Adversarial')
    plt.imshow(adversarial[:, :, ::-1] / 255)  # ::-1 to convert BGR to RGB
    plt.axis('off')

    plt.subplot(1, 3, 3)
    plt.title('Difference')
    difference = adversarial[:, :, ::-1] - image
    plt.imshow(difference / abs(difference).max() * 0.2 + 0.5)
    plt.axis('off')

    plt.show()
